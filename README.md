# matrix-docker [![GitLab CI Build Status](https://gitlab.com/mextrix/matrix-docker/badges/master/build.svg)](https://gitlab.com/mextrix/matrix-docker/pipelines)

This is a docker setup to run synapse and postgres inside docker, and our application service outside. To start it, simply run

```bash
sudo docker-compose up
```

Afterwards, you can use riot at http://localhost:8008/ to connect to your server.

## Pre-built images

To download a pre-built docker container instead of building it yourself, run **before** you start `docker-compose`:

```bash
sudo docker pull registry.gitlab.com/mextrix/matrix-docker/synapse
sudo docker pull registry.gitlab.com/mextrix/matrix-docker/riot
```

## Build the images

If you want to build the images yourself, feel free to do so:

```bash
sudo docker-compose build
```

## Clean State

To reset synapse as if it would be new, simply run the following commands (after stopping synapse):

```bash
sudo docker-compose down
sudo docker volume prune
```
